
from django.conf.urls.static                            import static
from django.contrib                                     import admin
from django.conf                                        import settings
from django.urls                                        import path, include


urlpatterns = [

    path('',                                    include('lupay.apps.web.views.home.urls')),
    path('agents/',                             include('lupay.apps.web.views.agents.urls')),
    path('devices/',                            include('lupay.apps.web.views.devices.urls')),
    path('payments/',                           include('lupay.apps.web.views.payments.urls')),

]
