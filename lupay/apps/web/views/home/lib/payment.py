
import os
from datetime                                           import timedelta

from django.conf                                        import settings
from django.db.models                                   import Q, F, Sum, ExpressionWrapper, DateField
from django.db.models.functions                         import TruncDate

from lupay.apps.web.db.payments.models                  import Payment
from .db_connect                                        import connect_db


def create_payment_records(i):

    directory = settings.CONFIG_DIR

    reports_path = directory + '/' + i['folder']

    if not os.path.exists(reports_path):
        os.mkdir(reports_path)

    bro_db = settings.DATABASES['default']

    conn = connect_db(bro_db)

    cur = conn.cursor()

    with open(f"{directory}/{i['file']}", 'r') as csv_file:
        next(csv_file)  # Skip the header row.
        cur.copy_from(csv_file, 'payments', sep=',')

    conn.commit()

    cur.close()
    conn.close()

    return


def drop_create_table(table):

    bro_db = settings.DATABASES['default']

    conn = connect_db(bro_db)

    cur = conn.cursor()

    cur.execute(f"DROP TABLE IF EXISTS {table};")

    conn.commit()

    query = """
    CREATE TABLE payments
        (
            id                      serial                   NOT NULL
                constraint payments_pkey
                    primary key,
            payment_type            varchar(50),
            payment_amount          numeric(15, 2),
            payment_signature_image varchar(150),
            payment_photo           varchar(150),
            created                 date NOT NULL,
            status                  varchar(50),
            notes                   varchar(250),
            agent_user_id           varchar(250),
            device_id               varchar(250)
        );
        
        ALTER TABLE payments
            owner to "lupay-user";

    """

    cur.execute(f"{query}")

    conn.commit()

    cur.close()
    conn.close()

    return


def days_from_suspension_report(date_to):

    payments = Payment.objects \
        .values(
            'device_id',
        ) \
        .annotate(
            days_from_suspension=ExpressionWrapper(
                TruncDate(F('created')) + timedelta(days=90) - date_to,
                output_field=DateField(),
            )
        ) \
        .order_by(
            'days_from_suspension'
        )

    return payments


def agent_collection_report():

    payments = Payment.objects\
        .values(
            'agent_user_id', 'created', 'payment_type'
        ) \
        .annotate(
            total_amount=Sum('payment_amount')
        ) \
        .order_by(
            'agent_user_id', 'created'
        )

    return payments


def payment_type_report():

    payments = Payment.objects\
        .values(
            'payment_type'
        ) \
        .annotate(
            total_amount=Sum('payment_amount')
        ) \
        .order_by(
            'payment_type'
        )

    return payments
