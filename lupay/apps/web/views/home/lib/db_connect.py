
import psycopg2


def connect_db(db):

    conn = psycopg2.connect(
        database = db['NAME'],
        user     = db['USER'],
        password = db['PASSWORD'],
        host     = db['HOST'],
        port     = db['PORT']
    )

    # print(f"Using DATABASE ==> '{db['NAME']}'")

    return conn
