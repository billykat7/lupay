import csv


def create_csv(**kwargs):

    reports_path          = kwargs.get('reports_path')
    days_from_suspentions = kwargs.get('days_from_suspention')
    agent_collections     = kwargs.get('agent_collection')
    payment_types         = kwargs.get('payment_type')

    days     = write_days_from_suspension_report(reports_path=reports_path, days_from_suspention=days_from_suspentions)
    agents   = agent_collection_report(reports_path=reports_path, agent_collection=agent_collections)
    payments = payment_type_report(reports_path=reports_path, payment_type=payment_types)

    return days, agents, payments


def write_days_from_suspension_report(**kwargs):

    reports_path                = kwargs.get('reports_path')
    days_from_suspentions       = kwargs.get('days_from_suspention')
    days_from_suspention_fields = ['device_id', 'days_from_suspension']

    with open(f'{reports_path}/days_from_suspension_report.csv', 'w', newline='', encoding='utf-8') as days_from_suspention_file:
        writer = csv.writer(days_from_suspention_file, delimiter=',')
        writer.writerow(days_from_suspention_fields)
        for days_from_suspention in days_from_suspentions:
            days_left = days_from_suspention['days_from_suspension'].days
            if days_left < 0:
                days_left = 0
                days_left = 'SUSPENDED'
            # days_left = str(days_left) + ' days'
            writer.writerow([days_from_suspention['device_id'], days_left])

    days_from_suspention_file.close()

    return


def agent_collection_report(**kwargs):

    reports_path            = kwargs.get('reports_path')
    agent_collections       = kwargs.get('agent_collection')
    agent_collection_fields = ['agent_user_id', 'date', 'payment_type', 'total_amount']

    with open(f'{reports_path}/agent_collection_report.csv', 'w', newline='', encoding='utf-8') as agent_collection_file:
        writer = csv.writer(agent_collection_file, delimiter=',')
        writer.writerow(agent_collection_fields)
        for agent_collection in agent_collections:
            writer.writerow(
                [
                    agent_collection['agent_user_id'],
                    agent_collection['created'],
                    agent_collection['payment_type'],
                    agent_collection['total_amount']
                ]
            )

    agent_collection_file.close()

    return


def payment_type_report(**kwargs):

    reports_path        = kwargs.get('reports_path')
    payment_types       = kwargs.get('payment_type')
    payment_type_fields = ['payment_type', 'total_amount']

    with open(f'{reports_path}/payment_type_report.csv', 'w', newline='', encoding='utf-8') as payment_type_file:
        writer = csv.writer(payment_type_file, delimiter=',')
        writer.writerow(payment_type_fields)
        for payment_type in payment_types:
            writer.writerow([payment_type['payment_type'], payment_type['total_amount']])

    payment_type_file.close()

    return
