import os, datetime

from django.conf                                    import settings
from django.contrib                                 import messages
from django.http import HttpResponse, Http404
from django.shortcuts                               import render
from django.views.generic                           import View

from .input                                         import input_get_input
from .lib                                           import csv      as csv_lib
from .lib                                           import payment  as payment_lib


# Create your views here.
class HomeView(View):
    template_name = 'index.html'

    def get(self, request, **kwargs):

        # i = input_get_input(self)
        #
        # context = {}

        return render(request, self.template_name, {})

    def post(self, request, **kwargs):

        time_start = datetime.datetime.now()

        i = input_get_input(self)

        directory = settings.CONFIG_DIR

        reports_path = directory + '/' + i['folder']

        if not os.path.exists(reports_path):
            os.mkdir(reports_path)

        table = 'payments'
        date_lst = i['file'].split('_')[:3]
        date_to = datetime.date(int(date_lst[0]), int(date_lst[1]), int(date_lst[2]))

        payment_lib.drop_create_table(table)

        payment_lib.create_payment_records(i)

        days_from_suspention = payment_lib.days_from_suspension_report(date_to)

        agent_collection     = payment_lib.agent_collection_report()

        payment_type         = payment_lib.payment_type_report()

        csv_lib.create_csv(
            reports_path=reports_path,
            days_from_suspention=days_from_suspention,
            agent_collection=agent_collection,
            payment_type=payment_type
        )

        messages.warning(request, "Reports successfully generated! Please Download")

        context = {
            'i': i,
        }

        time_end = datetime.datetime.now()
        duration = time_end - time_start
        print(f"duration={duration}")

        return render(request, self.template_name, context)


def days_from_suspension_report(request, **kwargs):

    folder = request.GET.get('folder')

    directory    = settings.CONFIG_DIR
    reports_path = directory + '/' + folder

    if os.path.exists(reports_path):
        with open(f"{reports_path}/days_from_suspension_report.csv", 'rb') as csv_file:
            response = HttpResponse(csv_file.read(), content_type="text/csv")
            response['Content-Disposition'] = 'attachment; filename="days_from_suspension_report.csv"'
            return response

    raise Http404


def agent_collection_report(request, **kwargs):

    folder = request.GET.get('folder')

    directory = settings.CONFIG_DIR
    reports_path = directory + '/' + folder

    if os.path.exists(reports_path):
        with open(f"{reports_path}/agent_collection_report.csv", 'rb') as csv_file:
            response = HttpResponse(csv_file.read(), content_type="text/csv")
            response['Content-Disposition'] = 'attachment; filename="agent_collection_report.csv"'
            return response

    raise Http404


def payment_type_report(request, **kwargs):

    folder = request.GET.get('folder')

    directory = settings.CONFIG_DIR
    reports_path = directory + '/' + folder

    if os.path.exists(reports_path):
        with open(f"{reports_path}/payment_type_report.csv", 'rb') as csv_file:
            response = HttpResponse(csv_file.read(), content_type="text/csv")
            response['Content-Disposition'] = 'attachment; filename="payment_type_report.csv"'
            return response

    raise Http404
