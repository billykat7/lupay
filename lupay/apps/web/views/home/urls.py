
from django.conf.urls.static                            import static
from django.contrib                                     import admin
from django.conf                                        import settings
from django.urls                                        import path, include

from lupay.apps.web.views.home.views                    import HomeView, days_from_suspension_report, \
                                                                agent_collection_report, \
                                                                payment_type_report

app_name = 'home'

urlpatterns = [

    path('',                        HomeView.as_view(),                     name='home'),
    path('suspensions/',            days_from_suspension_report,            name='suspensions'),
    path('agent_collection',        agent_collection_report,                name='agent_collection'),
    path('payments',                payment_type_report,                    name='payments'),

]
