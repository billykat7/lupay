
from django.conf.urls.static                            import static
from django.contrib                                     import admin
from django.conf                                        import settings
from django.urls                                        import path, include

from lupay.apps.web.views.agents.views                  import HomeView

app_name = 'agents'

urlpatterns = [

    path('',                HomeView.as_view(),                 name='home'),

]
