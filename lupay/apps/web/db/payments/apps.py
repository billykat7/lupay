from django.apps                                    import AppConfig


class PaymentsConfig(AppConfig):
    name = 'lupay.apps.web.db.payments'
