from decimal                                            import Decimal

from django.core.validators                             import MinValueValidator
from django.db                                          import models

from core.model_mixins                                  import AuditFields
from common.global_choices                              import PAYMENT_TYPE


# Create your models here.
class Payment(models.Model):

    # agent           = models.ForeignKey('agents.Agent',         on_delete=models.PROTECT, related_name='payments', null=True)
    # device          = models.ForeignKey('devices.Device',       on_delete=models.PROTECT, related_name='payments', null=True)

    payment_type            = models.CharField('PAYMENT TYPE', max_length=50, choices=PAYMENT_TYPE, blank=True, null=True)
    payment_amount          = models.DecimalField('AMOUNT PAID',        max_digits=15,  decimal_places=5,   null=True)
    payment_signature_image = models.CharField('PAYMENT SIGNATURE',     max_length=150,     blank=True,     null=True)
    payment_photo           = models.CharField('PAYMENT PHOTO',         max_length=150,     blank=True,     null=True)
    created                 = models.DateTimeField('DATETIME CREATED',  auto_now_add=True)
    status                  = models.CharField('STATUS',                max_length=50,      blank=True,     null=True)
    notes                   = models.CharField('PAYMENT NOTES',         max_length=250,     blank=True,     null=True)
    agent_user_id           = models.CharField('AGENT ID',              max_length=250,     blank=True,     null=True)
    device_id               = models.CharField('DEVICE ID',             max_length=250,     blank=True,     null=True)

    class Meta:

        app_label   = 'payments'
        db_table    = 'payments'
        verbose_name_plural = 'payments'
