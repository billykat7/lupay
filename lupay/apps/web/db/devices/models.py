from django.db                                          import models

from core.model_mixins                                  import AuditFields


# Create your models here.
class Device(models.Model):

    # agent           = models.ForeignKey('agents.Agent',         on_delete=models.PROTECT, related_name='devices', null=True)

    name    = models.CharField('NAME',          max_length=100,     blank=True,     null=True)
    status  = models.BooleanField('STATUS',     default=True)

    class Meta:

        app_label   = 'devices'
        db_table    = 'devices'
        verbose_name_plural = 'devices'
