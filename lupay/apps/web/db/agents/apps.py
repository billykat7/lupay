from django.apps                                    import AppConfig


class AgentsConfig(AppConfig):
    name = 'lupay.apps.web.db.agents'
