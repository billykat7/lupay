from django.core.validators import RegexValidator
from django.db                                          import models


# Create your models here.
USERNAME_REGEX     = '^[a-zA-Z0-9.+-]*$'


class Agent(models.Model):

    name        = models.CharField('AGENT NAME',    max_length=150,     blank=True,     null=True)
    username    = models.CharField('USERNAME',      max_length=100,     validators=[
        RegexValidator(
            regex=USERNAME_REGEX,
            message='Username must be alphanumeric or contains numbers.',
            code='invalid_username'
        )
    ], unique=True)
    password    = models.CharField('PASSWORD',      max_length=250)
    last_login  = models.DateTimeField('TIME LAST LOGIN',                       auto_now=True)
    is_active   = models.BooleanField('IS ACTIVE CHECK',                        default=True)

    class Meta:

        app_label   = 'agents'
        db_table    = 'agents'
        verbose_name_plural = 'agents'