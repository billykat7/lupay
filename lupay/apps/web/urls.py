
from django.conf.urls.static                            import static
from django.contrib                                     import admin
from django.conf                                        import settings
from django.urls                                        import path, include


urlpatterns = [

    path('',                                include('lupay.apps.web.views.urls')),

]
