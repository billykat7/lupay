
from django.conf.urls.static                            import static
from django.contrib                                     import admin
from django.conf                                        import settings
from django.urls                                        import path, include


urlpatterns = [

    # path('api/',                                   include('lupay.apps.api.urls')),
    path('',                                    include('lupay.apps.web.urls')),

]
