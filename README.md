# LuPay

Payment with Reports exported to CSV in Django

#The problem
----------------------------------------------------------------------------------------------
Payments.csv shows a list of payments based on device_id. We would like to use the payment ‘history’ to calculate the time that a client’s policy is valid for. This can be looked at as the total number of days until the client is suspended (Days from Suspension).
Once a payment has been made a client is covered for 30 days (this is the time during which any claims will be paid out). We know that clients will not always have the money on their exact payment date, and income is volatile, so after the 30 days of coverage period, the client enters into the grace period for 30 days. If a client misses their monthly payment they will go into the grace period. They can then make a payment to catch up. Furthermore, after the end of the grace period a client will still have 30 days of their suspension period. A client will only be suspended after 91 days of their first payment. Another way of looking at it, if a client is up to date on payments, a client is 90 days from suspension.
We have numerous products with different premiums, but the same logic applies to all of them.
We want to give our agents as much information possible to manage their clients. We want to be able to give agents a “filtered” list that shows their most pressing clients first. 

Furthermore, from the operational team perspective, we want two more reports:
    1. First collection per agent, per day, and per payment type
    2. Total collection per payment type
