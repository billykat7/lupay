
# PAYMENT TYPES
PAYMENT_TYPE = [
    ('CASH',            'CASH'),
    ('CLIENT_REFERRAL', 'CLIENT_REFERRAL'),
    ('CARD',            'CARD'),
    ('BANK_DEPOSIT',    'BANK_DEPOSIT'),
]
